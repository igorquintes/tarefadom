function enviarMsg(event) {
    event.preventDefault();

    let input = document.querySelector(".Nota");
    let escopo = document.querySelector(".DivNotas");

    // Verifica se o valor do campo "Nota:" está vazio
    if (input.value === "") {
        // Exibe a mensagem de erro
        alert("Por favor, insira uma nota");

    }
    // Verifica se o valor do campo "Nota:" é numérico
    else if (isNaN(input.value)) {
            // Exibe a mensagem de erro
            alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    }
    // Verifica se o valor do campo "Nota:" está entre 0 e 10
    else if (input.value < 0 || input.value > 10) {
        // Exibe a mensagem de erro
        alert("A nota digitada é inválida, por favor, insira uma nota válida.");
    } else {
        let texto = document.createElement("p");
        texto.innerText = input.value;
        escopo.appendChild(texto);
    }
}

function calcularMedia() {
    let notas = document.querySelectorAll(".DivNotas p");
    let soma = 0;
    let quantidade = notas.length;

    // Percorre todas as notas e soma os valores
    for (let i = 0; i < quantidade; i++) {
        soma += parseFloat(notas[i].innerText);
    }
    
    // Calcula a média
    let media = soma / quantidade;
    
    // Exibe a média
    let divMedia = document.querySelector(".Media");
    divMedia.innerText = "A média das notas é: " + media.toFixed(2);
}   

let btn_enviar = document.querySelector(".btn_Add");
btn_enviar.addEventListener("click", enviarMsg);

let btn_media= document.querySelector(".btn_Media");
btn_media.addEventListener("click", calcularMedia);
